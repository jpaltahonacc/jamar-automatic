const fetch = require('node-fetch');
const userdara = {};

userdara.searchUserCookies = async (req, res) => {
    const { value } = req.body;
    var query = {
        "requestIdentifierType": "COOKIE",
        "identifierType": "ADMANANALYTICS",
        "identifierValue": value
    }
    await fetch(`https://3kquh74ev6.execute-api.us-east-1.amazonaws.com/api/customerhistory`,{
        method: 'POST',
        headers: { 'x-api-key' : 'pSDco7u6pN22tNnZMWnj62kCSxBgp7CX2BhmjrEM' },
        body: JSON.stringify(query),
    })
    .then(res => res.json())
    .then(data => {
        const user = data[0].interactions;
        var userNavegations = [];
        var productView = [];
        var ProductsView = [];
        var page = [];
        user.map( ( iten) => {
            if(iten.eventDataPoint.eventData.items){
                productView.push(iten.eventDataPoint.eventData.items)
            }
            if(iten.eventDataPoint.eventData.page){
                page.push(iten.eventDataPoint.eventData.page)
            }
            userNavegations.push(iten.eventDataPoint.eventData);
        });
        productView.map( (item) => {
            ProductsView.push(item[0])
        } )
    
        var userData = {
            ProductsView,
            pagesView: page
        }

        res.json(userData);
    })
    .catch( (err) => {
        res.json({err})
    });     
}

module.exports = userdara;

