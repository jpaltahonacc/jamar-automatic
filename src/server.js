const express = require('express');
const bodyParser =  require("body-parser");
var cors = require('cors');
require('dotenv').config();

var port = process.env.PORT || 3000;

var corsOptions = {
  origin: 'https://www.jamar.com',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

require('dotenv').config();

const app = express();

// middlewares
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
  
//routes
app.get('/', (req, res)=> {
  res.send("hello")
});

app.use('/api/userdate/navegation', require('./routes/userData'));
app.use('/api/product', require('./routes/productData'));


app.listen(port, ()=> console.log(`server on port ${port}`));