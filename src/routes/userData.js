const express = require('express');
const router = express.Router();

const { searchUserCookies } = require ('../controllers/userDataCookies.controller');
const { searchUserCc } = require('../controllers/userDataCc.controller');
router.route('/cookies')
    .post(searchUserCookies)
router.route('/cc')
    .post(searchUserCc)

module.exports = router;